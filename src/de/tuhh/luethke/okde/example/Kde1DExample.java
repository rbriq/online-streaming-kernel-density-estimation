package de.tuhh.luethke.okde.example;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.ejml.simple.SimpleMatrix;
import org.math.plot.Plot2DPanel;

import de.tuhh.luethke.okde.Exceptions.EmptyDistributionException;
import de.tuhh.luethke.okde.model.BaseSampleDistribution;
import de.tuhh.luethke.okde.model.SampleModel;

/**
 * This is a simple example that illustrates the usage of the okde-java library.
 * It creates a set of two-dimensional sample points. The samples are generated
 * using a Gaussian mixture distribution with three components. Then the
 * okde-java library is used to estimate the sample distribution. Eventually,
 * the estimated distribution is plotted as a three-dimensional grid plot to
 * visualize the result.
 * 
 * @author jluethke
 * 
 */
public class Kde1DExample extends JFrame {

	private static final long serialVersionUID = -2880832753267205498L;
	private static final int DIM = 1;
	private static final int BOUND = 1;
	public Kde1DExample() {

		initUI();
	}

	private void initUI() {

		// some configuration for plotting
		setTitle("okde");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(900, 500);
		setLocationRelativeTo(null);

		// disable the forgetting factor
		double forgettingFactor = 1;
		// set the compression threshold
		double compressionThreshold = 0.01;
		// number of samples to genereate
		int noOfSamples = 1000;

		// sample model object used for sample distribution estimation
		SampleModel sampleDistribution = new SampleModel(forgettingFactor,
				compressionThreshold);
		
		SampleModel sampleDistribution1 = new SampleModel(forgettingFactor,
				compressionThreshold);

		// create a covariance matrix with all entries 0
		double[][] c = new double[DIM][DIM];
		for (int i = 0; i < c.length; i++) {
			for (int j = 0; j < c.length; j++) {
				c[i][j] = 0;
			}
		}

		// generate sample points using the rational function
		//RationalDistribution dist = new RationalDistribution(BOUND);
		GaussianPDF dist = new GaussianPDF(3.0, 0.5);
		GaussianPDF dist1 = new GaussianPDF(7.0, 0.2);
		
		SimpleMatrix[] samples = new SimpleMatrix[noOfSamples];
		SimpleMatrix[] samples1 = new SimpleMatrix[noOfSamples];
		Map<Double, Integer> realDistMap = new HashMap<Double, Integer>();
		// now generate the random samples
		for (int i = 0; i < noOfSamples; i++) {

			// double[][] sampleArray = { { gaussian(mean1[0], stddev1) }};
			double nextSample = dist.generateInstance()[0];
			double[][] sampleArray = { { nextSample } };

			double nextSample1 = dist1.generateInstance()[0];
			double[][] sampleArray1 = { { nextSample1 } };

			realDistMap.put(nextSample, realDistMap.getOrDefault(nextSample, 0) + 1);
			SimpleMatrix sample = new SimpleMatrix(sampleArray);
			SimpleMatrix sample1 = new SimpleMatrix(sampleArray1);
			samples[i] = sample;
			samples1[i] = sample1;

		}
		
		
		/*for (int i = noOfSamples/2; i < noOfSamples; i++) {

			// double[][] sampleArray = { { gaussian(mean1[0], stddev1) }};
			double nextSample = dist1.generateInstance()[0];
			double[][] sampleArray = { { nextSample } };

			double nextSample1 = dist1.generateInstance()[0];
			double[][] sampleArray1 = { { nextSample1 } };

			realDistMap.put(nextSample, realDistMap.getOrDefault(nextSample, 0) + 1);
			SimpleMatrix sample = new SimpleMatrix(sampleArray);
			SimpleMatrix sample1 = new SimpleMatrix(sampleArray1);
			samples[i] = sample;
			samples1[i] = sample1;
		}*/
		
		

		/*
		 * Now the sample model is updated using the generated sample data.
		 */
		try {
			// Add three samples at once to initialize the sample model
			ArrayList<SimpleMatrix> initSamples = new ArrayList<SimpleMatrix>();
			ArrayList<SimpleMatrix> initSamples1 = new ArrayList<SimpleMatrix>();
			initSamples.add(samples[0]);
			initSamples.add(samples[1]);
			initSamples.add(samples[2]);
			
			initSamples1.add(samples1[0]);
			initSamples1.add(samples1[1]);
			initSamples1.add(samples1[2]);
			
			double[] w = { 1, 1, 1 };
			SimpleMatrix[] cov = { new SimpleMatrix(c), new SimpleMatrix(c),
					new SimpleMatrix(c) };
			double[] w1 = { 1, 1, 1 };
			SimpleMatrix[] cov1 = { new SimpleMatrix(c), new SimpleMatrix(c),
					new SimpleMatrix(c) };
			sampleDistribution.updateDistribution(initSamples.toArray(new SimpleMatrix[3]), cov, w);
			sampleDistribution1.updateDistribution(initSamples1.toArray(new SimpleMatrix[3]), cov1, w1);
			// Update the sample model with all generated samples one by one.
			for (int i = 3; i < noOfSamples; i++) {
				SimpleMatrix pos = samples[i];
				SimpleMatrix pos1 = samples1[i];
				sampleDistribution.updateDistribution(pos, new SimpleMatrix(c),
						1d);
				sampleDistribution1.updateDistribution(pos1, new SimpleMatrix(c),
						1d);
			}
		} catch (EmptyDistributionException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		/*
		 * 
 the distribution estimated by the sample model
		 */

		// first create a 100x100 grid
		double[] xArray = new double[100];
		for (int i = 0; i < 100; i++) {
			xArray[i] = i * 0.1;
		}
		// then evaluate the sample model at each point of the grid
		
		SampleModel average = mergeDistributions(sampleDistribution1, sampleDistribution);
		

		/*try {
			// Update the sample model with all generated samples one by one.
			for (int i = 3; i < noOfSamples; i++) {
				SimpleMatrix pos = samples[i];
				SimpleMatrix pos1 = samples1[i];
				average.updateDistribution(pos, new SimpleMatrix(c), 1d);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} */
		
		
		System.out.println("sub dist size: " + average.getSubDistributions().size());
		double[] zArray = evaluateSampleDistribution(xArray, average);
		//double[] zArray = evaluateSampleDistribution(xArray, sampleDistribution1);
		//double[] zArray = evaluateSampleDistribution(xArray, sampleDistribution);

		Plot2DPanel plot = new Plot2DPanel("SOUTH");
		// add grid plot to the PlotPanel
		xArray = Arrays.stream(xArray).map(i -> i*BOUND).toArray();
		plot.addBarPlot("estimated sample distribution", xArray, zArray);
		Iterator<Entry<Double, Integer>> it = realDistMap.entrySet().iterator();
		double values [] = new double [realDistMap.keySet().size()];
		double counts [] = new double [realDistMap.keySet().size()];
		int i = 0;
		while(it.hasNext()) {
			Entry<Double, Integer> entry = it.next();
			values[i] = entry.getKey() * BOUND;
			counts[i] = (double)(entry.getValue())/noOfSamples;
			++i;
		}
		//plot.addBarPlot("actual distribution", values, counts);
		setContentPane(plot);
	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {

				Kde1DExample ps = new Kde1DExample();
				ps.setVisible(true);
			}
		});
	}

	/**
	 * Evaluates a given sample distribution at point in a given grid.
	 * 
	 * @param x
	 *            x-axis of the grid

	 * @param dist
	 *            sample distribution to evaluate
	 * @return z-components of all evaluated grid points
	 */
	private double[]evaluateSampleDistribution(double[] x,
			BaseSampleDistribution dist) {
		double[] z = new double[x.length];
		for (int i = 0; i < x.length; i++){
				double[][] point = { { x[i] } };
				SimpleMatrix pointVector = new SimpleMatrix(point);
				z[i] = dist.evaluate(pointVector);
			}
		return z;
	}
	
	
	
	
	private SampleModel mergeDistributions(SampleModel sampleDistribution1, SampleModel sampleDistribution2) {
		SampleModel average = null;
		try {
			average = new SampleModel(sampleDistribution1);
			average.mergeSampleModels(sampleDistribution2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return average;
	}

	
}