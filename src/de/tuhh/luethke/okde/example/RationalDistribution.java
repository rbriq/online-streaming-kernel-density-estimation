package de.tuhh.luethke.okde.example;

import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;
/**
 * This distribution represents a discrete probability distribution for the function 1/x
 * @author 
 *
 */
public class RationalDistribution extends ProbabilityDistibutionFunction<Double> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8256689588547776352L;

	private Random random = new Random();

	private int bound;
	
	private double normalFactor;
	
	private NavigableMap<Double,Integer> probabilities = new TreeMap<Double, Integer>();
	
	public RationalDistribution() {
		this(100);
	}

	public RationalDistribution(int bound) {
		this.bound = bound;
		double result = 0;
		for(int i =1; i<=bound;i++) {
			result += 1.0/i;
		}
		normalFactor = result;
		double prevProb = 0;
		double sum = 0;
		for(int i =1; i<=bound;i++) {
			double prob = 1.0/i;
			prob/=normalFactor;
			sum+= prob;
			probabilities.put(sum, i); // map i to its probability value
			prevProb = prob;
		}
	}
	@Override
	public Double[] generateInstance() {
		Double instance[] = new Double[dimension];
		for(int i =0; i<dimension; i++) {
			double uniformProb = random.nextDouble();
			int nextRnd = (probabilities.ceilingEntry(uniformProb)).getValue();
			instance[i] = (double)nextRnd/bound;
		}
 
		return instance;
	}

}
