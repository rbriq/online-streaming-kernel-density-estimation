package de.tuhh.luethke.okde.example;
import java.util.Random;

/**
 * represents a gaussian distribution function
 */
public class GaussianPDF extends ProbabilityDistibutionFunction<Double>{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1821212740429543741L;
	
	private Random random = new Random();

	private double mean = 0;
	// the standard deviation
	private double sigma = 1;
	
	public GaussianPDF() {
		
	}
	
	public GaussianPDF(double mean, double sigma){
		this.mean  = mean;
		this.sigma  = sigma;
	}

	@Override
	public Double[] generateInstance() {
		Double instance[] = new Double[dimension];
		for(int i =0; i<dimension; i++) {
			instance[i] = mean + sigma * random.nextGaussian();
		}
		return instance;
	}

}