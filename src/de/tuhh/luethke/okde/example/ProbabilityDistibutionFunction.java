package de.tuhh.luethke.okde.example;
public abstract class ProbabilityDistibutionFunction<T> implements Function<T> {

	private static final long serialVersionUID = 3899240729001389010L;
	protected int dimension;

	public ProbabilityDistibutionFunction() {
		dimension = 1; // assume a univariate pdf
	}
	
	public ProbabilityDistibutionFunction(int dimension) {
		this.dimension = dimension;
	}

	public abstract T[] generateInstance();

}